import {sendEvent} from "./WebSocketClient.js";
import { EventTypes } from "./EventTypes.js";

let fieldSize = 8;
let minesColor = "yellow";

export const generateNewField = (field) => {
    const gameContent = document.getElementById("gameContent");
    if(typeof(gameContent) != "undefined" && gameContent != null) {
        removeElement("gameContent");
    }
    const parent = document.getElementById("container");
    const content = getContainerDiv("gameContent", "gameContent");
    for (let i = 0; i < fieldSize; i++) {
        const row = getDiv('column');
        for (let j = 0; j < fieldSize; j++) {
            const index = i * fieldSize + j;
            const valueOnIndex = field[index];
            const clickable = valueOnIndex === " ";
            const event = {type: EventTypes.MAKE_MOVE, message: index};
            row.appendChild(getField(valueOnIndex === '0' ? '' : valueOnIndex, () => sendEvent(event), clickable ? "field" : "visibleField"));
        }
        content.appendChild(row);
    }
    parent.appendChild(content);
};

export const changeTime = (time) => {
    let formattedTime = (Math.round(time * 100) / 100);
    if(formattedTime < 0) {
        formattedTime = 0.0;
    }
    document.getElementById('timerText').innerHTML = formattedTime.toFixed(1);
};

export const generatePopup = (eventType) => {
    let message = "";
    if(eventType === EventTypes.YOU_WIN) {
        message = "YOU WIN";
        minesColor = "green";
    } else if(eventType === EventTypes.YOU_LOSE) {
        message = "YOU LOSE";
        minesColor = "red";
    } else if(eventType === EventTypes.DRAW) {
        message = "DRAW";
        minesColor = "yellow";
    }
    const parent = document.getElementById("container");
    const text = getTextDiv("resultText", message, 'popupText');
    text.classList.add(minesColor);
    const container = getDiv("popupContainer");
    container.appendChild(text);
    container.appendChild(getButton("find new opponent", findNewOpponent, 'bubbly-button'));
    parent.appendChild(container)
};

const findNewOpponent = () => {
    sendEvent({type: EventTypes.PLAY_AGAIN})
};

const getLoader = () => {
    const loader = getDiv("boxes");
    for(let i = 0; i < 4; i++) {
        const box = getDiv("box");
        for(let j = 0; j < 4; j++) {
            box.appendChild(getDiv())
        }
        loader.appendChild(box);
    }
    return loader
};

const getButton = (value, onclick, style) => {
    const button = document.createElement("input");
    button.setAttribute("type", "button");
    button.setAttribute("value", value);
    button.onclick = onclick;
    button.classList.add(style);
    return button;
};

const getDiv = (style) => {
    const newElement = document.createElement("div");
    newElement.classList.add(style);
    return newElement;
};

const getField = (value, onclick, style) => {
    const newElement = document.createElement("div");
    newElement.classList.add("field");
    newElement.classList.add(style);
    if(value !== '' && value !== ' ')
        newElement.classList.add(`field${value}`);
    newElement.onclick = onclick;
    newElement.innerHTML = value;
    return newElement;
};

const generateGameInfoBar = (gameInfo) => {
    fieldSize = gameInfo.fieldSize;
    const parent = document.getElementById("container");
    const barContainer = getContainerDiv("gameInfoBarContainer", "infoBarContainer");
    const userContainer = getTextDiv("userName", gameInfo.yourName, "infoBarItem");
    const opponentContainer = getTextDiv("opponentName", gameInfo.opponentsName, "infoBarItem");
    barContainer.appendChild(userContainer);
    barContainer.appendChild(getTextDiv("timerText", "-", 'timer'));
    barContainer.appendChild(opponentContainer);
    parent.appendChild(barContainer);
};

const getTextDiv = (id, text, style) => {
    const newElement = document.createElement("div");
    newElement.setAttribute("id", id);
    newElement.classList.add(style);
    newElement.innerHTML = text;
    return newElement;
};

const getContainerDiv = (id, style) => {
    const newElement = document.createElement("div");
    newElement.setAttribute("id", id);
    newElement.classList.add(style);
    return newElement;
};

export const changeTurn = (turn) => {
    if(turn === EventTypes.OPPONENT_MOVE) {
        document.getElementById('userName').classList.remove('move');
        document.getElementById('opponentName').classList.add('move');
    } else {
        document.getElementById('userName').classList.add('move');
        document.getElementById('opponentName').classList.remove('move');
    }
};

export const showGameField = (gameInfo) => {
    removeElement("container");
    const parent = document.getElementById("body");
    const newElement = document.createElement("div");
    newElement.setAttribute("id", "container");
    parent.appendChild(newElement);
    generateGameInfoBar(gameInfo)
};

const removeElement = (id) => {
    const element = document.getElementById(id);
    element.parentNode.removeChild(element);
};

export const showWaitingInformation = (text) => {
    removeElement("container");
    const parent = document.getElementById("body");
    const newElement = document.createElement("div");
    newElement.classList.add("waiting");
    newElement.setAttribute("id", "container");
    newElement.appendChild(getTextDiv("waitingText", text, "waitingText"));
    newElement.appendChild(getLoader());
    parent.appendChild(newElement);
};

const animateButton = (e) => {
    e.preventDefault();
    e.target.classList.remove('animate');

    e.target.classList.add('animate');
    setTimeout(sendName, 300);
    setTimeout(() => {
        e.target.classList.remove('animate');
    },700);
};

export const showNameInput = () => {
    removeElement("container");
    const parent = document.getElementById("body");
    const newElement = document.createElement("div");
    newElement.setAttribute("id", "container");
    newElement.classList.add("nameInputContent");
    const inputGroup = getDiv('inputGroup');
    const inputSpan = document.createElement("span");
    inputSpan.innerHTML = "Name";
    inputGroup.appendChild(inputSpan);
    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("id", "nameInput");
    input.classList.add("nameInput");
    inputGroup.appendChild(input);
    const button = document.createElement("button");
    button.innerHTML = "Play";
    button.classList.add("bubbly-button");
    button.addEventListener('click', animateButton, false);
    newElement.appendChild(inputGroup);
    newElement.appendChild(button);
    parent.appendChild(newElement);
};

const sendName = () => {
    const textInput = document.getElementById("nameInput");
    if(textInput.value.length === 0) {
        textInput.setCustomValidity("Field Required")
    } else {
        showWaitingInformation("Waiting for opponent ...");
        sendEvent({
           type: EventTypes.INTRODUCE_YOURSELF,
           message: textInput.value
        });
    }
};