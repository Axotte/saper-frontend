export class EventTypes {
    static MAKE_MOVE = "MAKE_MOVE";
    static YOUR_MOVE = "YOUR_MOVE";
    static OPPONENT_MOVE = "OPPONENT_MOVE";
    static GAME_CREATED = "GAME_CREATED";
    static GAME_STARTED = "GAME_STARTED";
    static INTRODUCE_YOURSELF = "INTRODUCE_YOURSELF";
    static WAITING_FOR_PLAYER = "WAITING_FOR_PLAYER";
    static YOU_LOSE = "YOU_LOSE";
    static YOU_WIN = "YOU_WIN";
    static DRAW = "DRAW";
    static PLAY_AGAIN = "PLAY_AGAIN";
    static TIME_CHANGED = "TIME_CHANGED";
    static OPPONENT_DISCONNECTED = "OPPONENT_DISCONNECTED";
}