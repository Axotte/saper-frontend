import { EventTypes } from "./EventTypes.js";
import { Subject } from 'https://dev.jspm.io/rxjs@6/_esm2015';
import { map, filter } from 'https://dev.jspm.io/rxjs@6/_esm2015/operators';
import {showWaitingInformation, showNameInput, showGameField,
    generateNewField, generatePopup, changeTurn, changeTime} from "./script.js";

const ws = new WebSocket("ws://192.168.43.113:8999");

const incomingEventSubject = new Subject();
const sendEventSubject = new Subject();
const incomingEventObservable = incomingEventSubject.pipe(map((event) => JSON.parse(event)));

export const sendEvent = (event) => {
    sendEventSubject.next(event);
};

const handleEndGameEvents = (event) => {
    generatePopup(event.type);
    generateNewField(event.message);
};

const handleFieldUpdate = (field) => {
    generateNewField(field);
};

const handleEvent = (event) => {
    if(event === EventTypes.WAITING_FOR_PLAYER) {
        showWaitingInformation("Waiting for opponent ...");
    } else if(event === EventTypes.GAME_CREATED) {
        showNameInput();
    }
};

const handleTimeChanged = (time) => {
    changeTime(time)
};

const handleGameStartedEvent = (gameInfo) => {
    showGameField(gameInfo)
};

const handleOpponentDisconnected = (_) => {
    sendEvent({type: EventTypes.PLAY_AGAIN})
};

const handleTurn = (turn) => {
    changeTurn(turn)
};

const sendToServer = (event) => {
    ws.send(event)
};

sendEventSubject
    .pipe(map((event) => JSON.stringify(event)))
    .subscribe(sendToServer);

incomingEventObservable
    .pipe(map((event) => event.type))
    .subscribe(handleEvent);

incomingEventObservable
    .pipe(filter((event) => event.type === EventTypes.TIME_CHANGED))
    .pipe(map((event) => event.message))
    .subscribe(handleTimeChanged);

incomingEventObservable
    .pipe(filter((event) => event.type === EventTypes.OPPONENT_DISCONNECTED))
    .subscribe(handleOpponentDisconnected);

incomingEventObservable
    .pipe(filter((event) => event.type === EventTypes.YOUR_MOVE || event.type === EventTypes.OPPONENT_MOVE))
    .pipe(map((event) => event.message))
    .subscribe(handleFieldUpdate);

incomingEventObservable
    .pipe(filter((event) => event.type === EventTypes.GAME_STARTED))
    .pipe(map((event) => event.message))
    .subscribe(handleGameStartedEvent);

incomingEventObservable
    .pipe(filter((event) => event.type === EventTypes.YOUR_MOVE || event.type === EventTypes.OPPONENT_MOVE))
    .pipe(map((event) => event.type))
    .subscribe(handleTurn);

incomingEventObservable
    .pipe(filter((event) => event.type === EventTypes.YOU_LOSE || event.type === EventTypes.YOU_WIN || event.type === EventTypes.DRAW))
    .subscribe(handleEndGameEvents);

ws.onmessage = (message) => {
    incomingEventSubject.next(message.data)
};

ws.onopen = () => {
    console.log("connection opened");
};


